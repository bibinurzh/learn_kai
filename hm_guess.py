import random
randomNum = random.randint(0,10)
count = 3
print("Guess number from 0 to 10. You have 3 attempts.")
while count > 0:
    userNum = int(input("User number: "))
    if userNum > randomNum:
        if count >1:
            print("Wrong. Try lower number.")
    elif userNum<randomNum:
        if count >1:
            print("Wrong. Try higher number.")
    else:
        print(f"You found it. The number was {randomNum}")
        break
    count =  count-1
    print(f"You have left {count} attempts")
if count==0:
    print(f"You have used all your attempts. The number was {randomNum}")
