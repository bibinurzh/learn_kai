from netmiko import ConnectHandler
device = {
    "device_type": "cisco_ios_telnet",
    "host": "learn.kai.kz",
    "username": "b.zhursinbek",
    "password": "123456",
    "port": 32773
}
net_connect = ConnectHandler(**device)
def get_config():

    file_name = input("Write file name: ")
    with open(file_name, "w") as file:
        output = net_connect.send_command("show run")
        file.write(output)
    print(f"Configuration has been backed up to {file_name}")

def change_address():
    new_loopback_ip = "10.10.10.1" 
    loopback_config_commands = [
        "interface loopback 0",
        f"ip address {new_loopback_ip} 255.255.255.255"
    ]

    net_connect.send_config_set(loopback_config_commands)
    net_connect.save_config() 

    print(f"Loopback 0 IP address changed to {new_loopback_ip}")
get_config()
change_address()
net_connect.disconnect()